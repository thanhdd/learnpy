from __future__ import print_function
import numpy
import torch
import time


#*************** TENSOR ***********************
# x = torch.empty(5,3)
# print('x: ', x)
# print('x shape: ', x.shape) # torch.Size([5, 3]) tuple
# m, n = x.shape
# print('m, n: ', m, n)
# print('x type: ', x.type)

# y = torch.rand(5, 3)
# print('y: ', y)

# z = torch.rand(5,3)
# print('z: ', z)

# a = torch.zeros(5, 3, dtype=torch.long)
# print('a: ', a)

# b = torch.tensor([5.5, 3])
# print('b: ', b)

# c = a.new_ones(5, 3, dtype=torch.double)
# print('c: ', c)

# d = torch.randn_like(a, dtype=torch.float)
# print('d: ', d)

# ************************************ OPERATIONS ******************************
# x = torch.rand(5, 3)
# y = torch.rand(5, 3)
# print('x: ', x)
# print('y: ', y)
# print('x + y: ', x + y)

# print('torch.add(x, y): ', torch.add(x, y))

# result = torch.empty(5, 3)
# torch.add(x, y, out=result)
# print('result: ', result)

# y.add_(x) # add_ change y
# print('y: ', y)

# # SLICE
# print('x: ', x)
# print('x[:, 1]: ', x[:, 1])

# RESHAPE
# x = torch.randn(4, 4)
# y= x.view(16)
# z = x.view(-1, 8)
# print('x: ', x)
# print('y: ', y)
# print('z: ', z)

# x = torch.randn(1)
# print('x: ', x)
# print('x item: ', x.item()) # to scalar

# # NUMPY BRIDGE
# a = torch.ones(5)
# print('a: ', a)

# b = a.numpy()
# print('b: ', b)
# # print( a + b)
# a.add_(1)
# print('a: ', a)
# print('b: ', b)

# a = numpy.ones(5)
# b = torch.from_numpy(a)

# print('a: ', a)
# print('b: ', b)

# numpy.add(a, 1, out=a)
# print('a: ', a)
# print('b: ', b)

#*************************** CUDA TENSORS ***********************
# if torch.cuda.is_available():
#   device = torch.device('cuda')
#   x = torch.tensor(.9806)
#   y = torch.ones_like(x, device=device)
#   x = x.to(device)
#   z = x + y
#   print(z)
#   print(z.to('cpu', torch.double))

#*************************** AUTOGRAD: AUTOMATIC DIFFERENTIATION ***************************
x = torch.ones(2, 2, requires_grad=True)
print('x: ', x)

# y = x + 2
# print('y: ', y)
# # y.requires_grad = True
# print('y grad function: ', y.grad_fn)
# z = y * y * 3
z = x + 2
out = z.mean()
print('z, out: ', z, out)

out.backward()
# print(out.grad)
# print(z.grad)
# # print(y.grad)
print(x.grad)

x = torch.randn(3, requires_grad=True)
print('x: ', x)

y = x * 2
print('y: ', y)
print('y data: ', y.data)
print('y data norm: ', y.data.norm())
print('y data norm fro: ', y.data.norm('fro'))
while y.data.norm() < 1000:
  y = y * 2

print('y: ', y)


#*************** NUMPY NEURAL NETWORK ***********************

# N, D_in, H, D_out = 64, 1000, 100, 10

# x = numpy.random.randn(N, D_in)
# y = numpy.random.randn(N, D_out)

# w1 = numpy.random.randn(D_in, H)
# w2 = numpy.random.randn(H, D_out)

# learning_rate = 1e-6
# for t in range(500):
#   h = x @ w1
#   h_relu = numpy.maximum(h, 0)
#   y_predict = h_relu @ w2

#   loss = numpy.square(y_predict - y).sum()
#   print(t, loss)

#   grad_y_pred = 2.0 * (y_predict - y)

#   grad_w2 = h_relu.T @ grad_y_pred
#   grad_h_relu = grad_y_pred @ w2.T 

#   grad_h = grad_h_relu.copy()
#   grad_h[h < 0] = 0

#   grad_w1 = x.T @ grad_h
#   grad_x = grad_h @ w1.T

#   w1 -= learning_rate * grad_w1
#   w2 -= learning_rate * grad_w2


#***************************** PYTORCH NEURAL NETWORK *******************************

# dtype = torch.float
# # device = torch.device("cpu")
# device = torch.device("cuda:0")

# N, D_in, H, D_out = 64, 1000, 100, 10

# x = torch.randn(N, D_in, device=device, dtype=dtype)
# y = torch.randn(N, D_out, device=device, dtype=dtype)

# w1 = torch.randn(D_in, H, device=device, dtype=dtype, requires_grad=False)
# w2 = torch.randn(H, D_out, device=device, dtype=dtype, requires_grad=False)

# learning_rate = 1e-6

# begin = time.time()
# for t in range(500):
#   # h = x.mm(w1)
#   h = x @ w1
#   h_relu = h.clamp(min=0)
#   # y_pred = h_relu.mm(w2)
#   y_pred = h_relu @ w2

#   loss = (y_pred - y).pow(2).sum().item()
#   # print(t, loss)

#   grad_y_pred = 2.0 * (y_pred - y)
#   # grad_w2 = h_relu.t().mm(grad_y_pred)
#   grad_w2 = h_relu.t() @ grad_y_pred
#   # grad_h_relu = grad_y_pred.mm(w2.t())
#   grad_h_relu = grad_y_pred @ w2.t()
#   grad_h = grad_h_relu.clone()
#   grad_h[h < 0] = 0
#   # grad_w1 = x.t().mm(grad_h)
#   grad_w1 = x.t() @ grad_h

#   w1 -= learning_rate * grad_w1
#   w2 -= learning_rate * grad_w2

# end = time.time()

# benchmark = end - begin
# print('CPU benchmark: ', benchmark)

#***************************** AUTOGRAD *********************************

# dtype = torch.float
# device = torch.device("cuda")

# N, D_in, H, D_out = 64, 1000, 100, 10

# x = torch.randn(N, D_in, device=device, dtype=dtype)
# y = torch.randn(N, D_out, device=device, dtype=dtype)

# w1 = torch.randn(D_in, H, device=device, dtype=dtype, requires_grad=True)
# w2 = torch.randn(H, D_out, device=device, dtype=dtype, requires_grad=True)

# learning_rate = 1e-6

# begin = time.time()
# for t in range(500):
#   y_pred = (x @ w1).clamp(min=0) @ w2

#   loss = (y_pred - y).pow(2).sum()
#   print(t, loss.item())

#   loss.backward()

#   with torch.no_grad():
#     w1 -= learning_rate * w1.grad
#     w2 -= learning_rate * w2.grad

#     w1.grad.zero_()
#     w2.grad.zero_()
# end = time.time()

# benchmark = end - begin
# print('CPU benchmark: ', benchmark)


#****************************** CUSTOMIZE AUTOGRAD *************************************

# class MyReLU(torch.autograd.Function):

#   @staticmethod
#   def forward(ctx, input):
#     ctx.save_for_backward(input)
#     return input.clamp(min=0)

#   @staticmethod
#   def backward(ctx, output):
#     input, = ctx.saved_tensors
#     grad_input = grad_output.clone()
#     grad_input[input < 0] = 0
#     return grad_input

# dtype = torch.float
# device = torch.device("cuda")

# N, D_in, H, D_out = 64, 1000, 100, 10

# x = torch.randn(N, D_in, device=device, dtype=dtype)
# y = torch.randn(N, D_out, device=device, dtype=dtype)

# w1 = torch.randn(D_in, H, device=device, dtype=dtype, requires_grad=True)
# w2 = torch.randn(H, D_out, device=device, dtype=dtype, requires_grad=True)

# learning_rate = 1e-6
# for t in range(500):
#   relu = MyReLU.apply
#   y_pred = relu(x @ w1) @ w2
  
#   loss = (y_pred - y).pow(2).sum()

#   with torch.no_grad():
#     w1 -= learning_rate * w1.grad
#     w2 -= learning_rate * w2.grad

#     w1.grad.zero()
#     w2.grad.zero()

#************************************* NN MODULE **********************************************
# N, D_in, H, D_out = 64, 1000, 100, 10

# x = torch.randn(N, D_in)
# y = torch.randn(N, D_out)

# model = torch.nn.Sequential(
#                             torch.nn.Linear(D_in, H),
#                             torch.nn.ReLU(),
#                             torch.nn.Linear(H, D_out),
#                             )
# loss_function = torch.nn.MSELoss(reduction='sum') # Mean least square, sum((predictions - y)^2)/n

# learning_rate = 1e-4

# for t in range(1000):
#   y_pred = model(x)

#   loss = loss_function(y_pred, y)
#   print(t, loss.item())

#   model.zero_grad()

#   loss.backward()
  
#   with torch.no_grad():
#     for param in model.parameters():
#       param -= learning_rate * param.grad

# ************************************ OPTIM *************************************
# N, D_in, H, D_out = 64, 1000, 100, 10

# x = torch.randn(N, D_in)
# y = torch.randn(N, D_out)

# model = torch.nn.Sequential(
#                             torch.nn.Linear(D_in, H),
#                             torch.nn.ReLU(),
#                             torch.nn.Linear(H, D_out),
#                             )

# loss_function = torch.nn.MSELoss(reduction='sum')

# learning_rate = 1e-4

# optimizer = torch.optim.Adam(model.parameters(), lr=learning_rate)

# for i in range(500):
#   y_pred = model(x)

#   loss = loss_function(y_pred, y)
#   print(t, loss.item())

#   optimizer.zero_grad()

#   loss.backward()
#   optimizer.step()

# ************************************** CUSTOM NN MODELS *******************************************
# class TwoLayerNet(torch.nn.Module):
#   def __init__(self, D_in, H, D_out):
#     super(TwoLayerNet, self).__init__()
#     self.linear1 = torch.nn.Linear(D_in, H)
#     self.linear2 = torch.nn.Linear(H, D_out)

#   def forward(self, x):
#     h_relu = self.linear1(x).clamp(min=0)
#     y_pred = self.linear2(h_relu)
#     return y_pred

# N, D_in, H, D_out = 64, 1000, 100, 10

# x = torch.randn(N, D_in)
# y = torch.randn(N, D_out)

# model = TwoLayerNet(D_in, H, D_out)

# criterion = torch.nn.MSELoss(reduction='sum')
# optimizer = torch.optim.SGD(model.parameters(), lr=1e-4)

# for t in range(500):
#   y_pred = model(x)
#   loss = criterion(y_pred, y)
#   print(t, loss.item())

#   optimizer.zero_grad()
#   loss.backward()
#   optimizer.step()
# ************************************** CONTROL FLOW & WEIGHT SHARING **************************************
# import random

# class DynamicNet(torch.nn.Module):
#   def __init__(self, D_in, H, D_out):
#     super(DynamicNet, self).__init__()
#     self.input_linear = torch.nn.Linear(D_in, H)
#     self.middle_linear = torch.nn.Linear(H, H)
#     self.output_linear = torch.nn.Linear(H, D_out)

#   def forward(self, x):
#     h_relu = self.input_linear(x).clamp(min=0)
#     for i in range(random.randint(0,3)):
#       h_relu = self.middle_linear(h_relu).clamp(min=0)
#     y_pred = self.output_linear(h_relu)
#     return y_pred

# N, D_in, H, D_out = 64, 1000, 100, 10

# x = torch.randn(N, D_in)
# y = torch.randn(N, D_out)

# model = DynamicNet(D_in, H, D_out)

# criterion = torch.nn.MSELoss(reduction='sum')
# optimizer = torch.optim.SGD(model.parameters(), lr=1e-4, momentum=0.9)

# for t in range(500):
#   y_pred = model(x)

#   loss = criterion(y_pred, y)

#   print(t, loss.item())

#   optimizer.zero_grad()
#   loss.backward()
#   optimizer.step()
  