import tensorflow
import numpy
import matplotlib.pyplot as pyplot
import utils
import time
import os
import cifar

def batch_data(X, y, batch_size):
    X_batch = numpy.random.permutation(X)[:batch_size]
    y_batch = numpy.random.permutation(y)[:batch_size]
    return X_batch, y_batch

def to_matrix(y):
  N = len(y)
  Y = numpy.zeros((N, 10))
  Y[range(N), y] = 1
  return Y

def next_batch(X, Y, batch_size):

  idx = numpy.arange(0, X.shape[0])
  numpy.random.shuffle(idx)
  idx = idx[:batch_size]
  data_shuffle = [X[i] for i in idx]
  labels_shuffle = [Y[i] for i in idx]
  return numpy.asarray(data_shuffle), numpy.asarray(labels_shuffle)

#get data
X_train, y_train = cifar.get_train_data()
# print('X shape, y shape', X_train.shape, y_train.shape)
X_train = X_train.T # 50000 x 3072
Y_train = to_matrix(y_train)

X_test, y_test = cifar.get_test_data()
# print('X shape, y shape', X_test.shape, y_test.shape)
X_test = X_test.T # 10000 x 3072
Y_test = to_matrix(y_test)

learning_rate = 0.01
num_steps = 100
# display_step = 100
batch_size = 128

# build model
D_in, H1, H2, D_out = 3072, 512, 512, 10

X = tensorflow.placeholder("float", [None, D_in])
Y = tensorflow.placeholder("float", [None, D_out])

weights = {
    'h1': tensorflow.Variable(tensorflow.random_normal([D_in, H1])),
    'h2': tensorflow.Variable(tensorflow.random_normal([H1, H2])),
    'out': tensorflow.Variable(tensorflow.random_normal([H2, D_out]))
}
biases = {
    'b1': tensorflow.Variable(tensorflow.random_normal([H1])),
    'b2': tensorflow.Variable(tensorflow.random_normal([H2])),
    'out': tensorflow.Variable(tensorflow.random_normal([D_out]))
}


layer_1 = tensorflow.add(tensorflow.matmul(X, weights['h1']), biases['b1'])

layer_2 = tensorflow.add(tensorflow.matmul(layer_1, weights['h2']), biases['b2'])

logits = tensorflow.matmul(layer_2, weights['out']) + biases['out']


loss_op = tensorflow.reduce_mean(tensorflow.nn.softmax_cross_entropy_with_logits_v2(
    logits=logits, labels=Y))

optimizer = tensorflow.train.AdamOptimizer(learning_rate=learning_rate)

train_op = optimizer.minimize(loss_op)
 

correct_pred = tensorflow.equal(tensorflow.argmax(logits, 1), tensorflow.argmax(Y, 1))
accuracy = tensorflow.reduce_mean(tensorflow.cast(correct_pred, tensorflow.float32))
 

init = tensorflow.global_variables_initializer()

with tensorflow.Session() as sess:

    sess.run(init)
 
    for step in range(1, num_steps+1):
        batch_x, batch_y = next_batch(X_train, Y_train, batch_size)
        # Run optimization op (backprop)
        sess.run(train_op, feed_dict={X: batch_x, Y: batch_y})

        # if step % display_step == 0 or step == 1:
            # Calculate batch loss and accuracy
        loss, acc = sess.run([loss_op, accuracy], feed_dict={X: batch_x,
                                                                 Y: batch_y})
        print("Step " + str(step) + ", Minibatch Loss= " + \
                  "{:.4f}".format(loss) + ", Training Accuracy= " + \
                  "{:.3f}".format(acc))
 
    print("Optimization Finished!")
    

    print("Testing Accuracy:", \
        sess.run(accuracy, feed_dict={X: X_test,
                                      Y: Y_test}))



