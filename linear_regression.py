# linear_regression.py
import numpy
import matplotlib.pyplot as pyplot
import time

def pause():
  wait = raw_input("PROGRAM PAUSED. PRESS ENTER TO CONTINUE.")
  print("CONTINUE..,")

def compute_cost(X, y, weight):
  m = len(y)
  J = numpy.sum(numpy.square(X.dot(weight) - y))/2/m
  return J

def gradient_descent(X, y, weight, alpha, num_iters):
  m = X.shape[0]
  J_history = numpy.zeros((num_iters))

  for iter in range(num_iters - 1):
    weight = weight -  alpha * X.T.dot(X.dot(weight) - y) / m

    # return weight 
    J_history[iter] = compute_cost(X, y, weight)

  return weight, J_history
# pause()
# A = numpy.array([])
# A = numpy.eye(5)
# print 'A', A

data = numpy.loadtxt('ex1data1.txt', delimiter=',')
X = data[:,0]; y = data[:,1:2]
m = len(y) # number of training examples

pyplot.plot(X, y, 'rx', markersize=6)
pyplot.xlabel('Profit in $10 000s')
pyplot.ylabel('Population of City in 10 000s')
pyplot.show()

X = numpy.array([numpy.ones((m)), data[:,0]]).T
# print X.shape
theta = numpy.zeros((2,1))
iterations = 1501
alpha = 0.01

J = compute_cost(X, y, theta)
print 'Cost1 : ', J

J = compute_cost(X, y, numpy.array([-1,2]).reshape(2,1))
print 'Cost 2: ', J

print 'Run Gradient Descent...'

start = time.time()
theta, J_history = gradient_descent(X, y, theta, alpha, iterations)
y_predict = X.dot(theta)
end = time.time()
print 'Linear Regression Benchmark: ', start - end

print 'Gradient Descent Theta: ', theta
theta_best = numpy.linalg.pinv(X).dot(y)
print 'Pseudo Inverse Theta: ',theta_best

print J_history[-10:-1]

pyplot.plot(X[:,1], y, 'rx', markersize=6)
pyplot.plot(X[:,1], X.dot(theta), 'b-',linewidth=0.2)
pyplot.plot(X[:,1], X.dot(theta_best), 'g-',linewidth=0.2)

pyplot.xlabel('Profit in $10 000s')
pyplot.ylabel('Population of City in 10 000s')
pyplot.legend(['Training data', 'Linear regression','Psedudo Inverse'])
pyplot.show()