# data_process.py
from __future__ import print_function, division
import os
import torch
import pandas
from skimage import io, transform
import numpy
import matplotlib.pyplot as pyplot
from torch.utils.data import Dataset, DataLoader
from torchvision import transforms, utils

import warnings
warnings.filterwarnings("ignore")

pyplot.ion()

landmarks_frame = pandas.read_csv('data/faces/face_landmarks.csv')

n = 1
img_name = landmarks_frame.iloc[n, 0]
landmarks = landmarks_frame.iloc[n, 1:].as_matrix()
landmarks = landmarks.astype('float').reshape(-1, 2)

print('Image name: {}'.format(img_name))
print('Landmarks shape: {}'.format(landmarks.shape))
print('First 4 Landmarks: {}'.format(landmarks[:4]))

def show_landmarks(image, landmarks):
  pyplot.imshow(image)
  pyplot.scatter(landmarks[:, 0], landmarks[:, 1], s=10, marker='.', c='r')
  pyplot.pause(5)
  pyplot.show()

pyplot.figure()
show_landmarks(io.imread(os.path.join('data/faces/', img_name)), landmarks)
# pyplot.show()