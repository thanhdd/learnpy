# test_tensorflow.py
# Creates a graph.
# import tensorflow as tf
import tensorflow
import numpy
import matplotlib.pyplot as pyplot


# a = tensorflow.add(3, 5) # just operator object

# print(a)

# sess = tensorflow.Session()
# sess.run(a)
# sess.close()
# print('********************** a ', a)


# with tensorflow.Session() as sess:
#   print(sess.run(a))

# x = 2
# y = 3 
# op1 = tensorflow.add(x, y)
# op2 = tensorflow.multiply(x, y)
# op3 = tensorflow.pow(op2, op1)
# with tensorflow.Session() as sess:
#   op3 = sess.run(op3)
#   print('Op3: ', op3)

# add_op = tensorflow.add(x, y)
# mul_op = tensorflow.multiply(x, y)
# useless = tensorflow.multiply(x, add_op)
# pow_op = tensorflow.pow(add_op, mul_op)

# with tensorflow.Session() as sess:
#   z = sess.run(pow_op)
#   print('Z: ', z)

# with tensorflow.Session() as sess:
#   z, not_useless = sess.run([pow_op, useless])

# print('Z, not useless: ', z, not_useless)


# with tensorflow.device('/gpu:0'):
#   a = tensorflow.constant([1.0, 2.0, 3.0, 4.0, 5.0, 6.0], name='a')
#   b = tensorflow.constant([1.0, 2.0, 3.0, 4.0, 5.0, 6.0], name='b')
#   c = tensorflow.multiply(a, b)

# # Creates a session with log_device_placement set to True.
# sess = tensorflow.Session(config=tensorflow.ConfigProto(log_device_placement=True))

# # Runs the op.
# print(sess.run(c))

# graph = tensorflow.Graph()
# graph = tensorflow.get_default_graph()
# with graph.as_default():
#   x = tensorflow.add(3, 5)
# sess = tensorflow.Session(graph=graph)
# # print('Run a graph: ', sess.run(x))
# with tensorflow.Session() as sess:
#   sess.run(x)
#   print('Run a graph: ', sess.run(x))

# graph = tensorflow.Graph()

# a = tensorflow.constant(3)

# with graph.as_default():
#   b = tensorflow.constant(5)

# g1 = tensorflow.get_default_graph()
# g2 = tensorflow.Graph()

# with g1.as_default():
#   a = tensorflow.constant(3)

# with g2.as_default():
#   b = tensorflow.constant(5)
import os
os.environ['TF_CPP_MIN_LOG_LEVEL']='2' # remove warning

# a = tensorflow.constant(2)
# b = tensorflow.constant(3)
# x = tensorflow.add(a, b)

# # writer = tensorflow.summary.FileWriter('./graphs', tensorflow.get_default_graph())

# with tensorflow.Session() as sess:
#   writer = tensorflow.summary.FileWriter('./graphs', sess.graph)
#   print(sess.run(x))
# writer.close

# a = tensorflow.constant(2, name='a')
# b = tensorflow.constant(3, name='b')
# x = tensorflow.add(a,b, name='add')

# writer = tensorflow.summary.FileWriter('./graphs', tensorflow.get_default_graph())
# with tensorflow.Session() as sess:
#   print(sess.run(x))
# writer.close

# a = tensorflow.constant([2, 2], name='a')
# b = tensorflow.constant([[0, 1], [2, 3]], name='b')
# x = tensorflow.multiply(a, b, name='mul')

# with tensorflow.Session() as sess:
#   print(sess.run(x))
#   print(sess.run(tensorflow.div(b, a)))
#   print(sess.run(tensorflow.divide(b, a)))
#   print(sess.run(tensorflow.truediv(b, a)))
#   print(sess.run(tensorflow.floordiv(b, a)))
#   # print(sess.run(tensorflow.realdiv(b, a)))
#   print(sess.run(tensorflow.truncatediv(b, a)))
#   print(sess.run(tensorflow.floor_div(b, a)))

# t0 = 19
# print(tensorflow.zeros_like(t0))
# print(tensorflow.ones_like(t0))

# t1 = [b"apple", b"peach", b"grape"]
# print(tensorflow.zeros_like(t1))
# # print(tensorflow.ones_like(t1))

# t2 = [[True, False, False],
#       [False, False, True],
#       [False, True, False]]

# print(tensorflow.zeros_like(t2))
# print(tensorflow.ones_like(t2))

# tensorflow.int32 == numpy.int32

# tensorflow.ones([2,2], numpy.float32)

# sess = tensorflow.Session()
# a = tensorflow.zeros([2, 3], numpy.int32)
# print(type(a))

# s = tf.get_variable("scalar", initializer=tf.constant(2)) 
# m = tf.get_variable("matrix", initializer=tf.constant([[0, 1], [2, 3]]))
# W = tf.get_variable("big_matrix", shape=(784, 10), initializer=tf.zeros_initializer())

# with tf.Session() as sess:
#     # print(sess.run(tf.global_variables_initializer()))
#     # sess.run(tf.variables_initializer([s,m]))
#     sess.run(W.initializer)

# a = tf.placeholder(tf.float32, shape=[3])
# b = tf.constant([5,5,5], tf.float32)

# c = a + b

# with tf.Session() as sess:
#   print(sess.run(c, feed_dict={a: [1,2,3]}))

# x = tf.Variable(10, name='x')
# y = tf.Variable(20, name='y')
# z = tf.add(x, y)

# writer = tf.summary.FileWriter('./graphs/normal_loading', tf.get_default_graph())

# with tf.Session() as sess:
#   sess.run(tf.global_variables_initializer())
#   for _ in range(10):
#     sess.run(z)
# writer.close

# Lazy loading
# x = tf.Variable(10, name='x')
# y = tf.Variable(20, name='y')

# writer = tf.summary.FileWriter('./graphs/normal_loading', tf.get_default_graph())
# with tf.Session() as sess:
#     sess.run(tf.global_variables_initializer())
#     for _ in range(10):
#     sess.run(tf.add(x, y)) # someone decides to be clever to save one line of code
# writer.close()

#**************************** LINEAR REGRESSION **************************************

# X_train = numpy.asarray([3.3, 4.4, 5.5, 6.71, 6.93, 4.168, 9.779, 6.182, 7.59, 2.167,
#                       7.042, 10.791, 5.313, 7.997, 5.654, 9.27, 3.1])

# Y_train = numpy.asarray([1.7, 2.76, 2.09, 3.19, 1.694, 1.573, 3.366, 2.596, 2.53, 1.221,
#                       2.827, 3.465, 1.65, 2.904, 2.42, 2.94, 1.3])
# n_samples = X_train.shape[0]

# X = tensorflow.placeholder(tensorflow.float32, name='X')
# Y = tensorflow.placeholder(tensorflow.float32, name='Y')

# w = tensorflow.get_variable('weights', initializer=tensorflow.constant(0.0))
# b = tensorflow.get_variable('bias', initializer=tensorflow.constant(0.0))

# Y_predicted = w *  X + b

# loss = tensorflow.square(Y - Y_predicted, name='loss')

# optimizer = tensorflow.train.GradientDescentOptimizer(learning_rate = 0.001).minimize(loss)
# writer = tensorflow.summary.FileWriter('./graphs', tensorflow.get_default_graph())

# with tensorflow.Session() as sess:
#   sess.run(tensorflow.global_variables_initializer())

#   for i in range(100):
#     total_loss = 0
#     for x, y in zip(X_train, Y_train):
#       _, _loss = sess.run([optimizer, loss], feed_dict={X: x, Y: y})
#       total_loss += _loss
#     print('Epock {0}: {1}'.format(i, total_loss / n_samples))

#   w_out, b_out = sess.run([w,b])


# Y_pred = X_train * w_out + b_out

# for i, j in zip(Y_pred, Y_train):
#   print(i, '|', j)


# pyplot.plot(X_train, Y_train, 'rx', markersize=6)
# pyplot.plot(X_train, Y_pred, 'b-',linewidth=0.2)

# pyplot.xlabel('X')
# pyplot.ylabel('Y')
# pyplot.legend(['Training data', 'Linear regression'])
# pyplot.show()

#************************ LOGISTIC REGRESSION *********************************










