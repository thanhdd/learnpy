# keras_example.py
import tensorflow
from tensorflow import keras
from tensorflow.keras import layers
import numpy
import matplotlib.pyplot as pyplot

N, D_in, H, D_out = 64, 1000, 100, 10

x = numpy.random.randn(N, D_in)
y = numpy.random.randn(N, D_out)
model = keras.Sequential()
model.add(layers.Dense(100, activation='relu'))
model.add(layers.Dense(10))
# model.add()
model.compile(optimizer=tensorflow.train.AdamOptimizer(0.0001),
              loss='categorical_crossentropy',
              metrics=['accuracy'])

model.fit(x, y, epochs=10)

