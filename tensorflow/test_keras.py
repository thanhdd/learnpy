# test_keras.py
import tensorflow
from tensorflow import keras
import numpy
import matplotlib.pyplot as pyplot


print(tensorflow.VERSION)
print(tensorflow.keras.__version__)

fashion_mnist = keras.datasets.fashion_mnist
# print(keras.datasets) 

(train_images, train_labels), (test_images, test_labels) = fashion_mnist.load_data()
class_names = ['T-shirt/top', 'Trouser', 'Pullover', 'Dress', 'Coat', 'Sandal', 'Shirt', 'Sneaker', 'Bag', 'Ankle boot']

print(train_images.shape)
print(len(train_labels))
print(len(test_labels))
pyplot.figure()
pyplot.imshow(train_images[0])
pyplot.colorbar()
pyplot.grid(False)
# pyplot.show()

train_images = train_images / 255.0
test_images = test_images / 255.0

pyplot.figure(figsize=(10,10))
for i in range(25):
  pyplot.subplot(5,5, i+1)
  pyplot.xticks([])
  pyplot.yticks([])
  pyplot.grid(False)
  pyplot.imshow(train_images[i], cmap=pyplot.cm.binary)
  pyplot.xlabel(class_names[train_labels[i]])
# pyplot.show()


model = keras.Sequential([
                          keras.layers.Flatten(input_shape=(28, 28)),
                          keras.layers.Dense(128, activation=tensorflow.nn.relu),
                          keras.layers.Dense(10, activation=tensorflow.nn.softmax)])
model.compile(optimizer=tensorflow.train.AdamOptimizer(),
              loss='sparse_categorical_crossentropy',
              metrics=['accuracy'])
model.fit(train_images, train_labels, epochs=1)

test_loss, test_acc = model.evaluate(test_images, test_labels)
print('Test accuracy: ', test_acc)

predictions = model.predict(test_images)
print(predictions[0])

print(numpy.argmax(predictions[0]))
print(test_labels[0])

def plot_image(i, predictions_array, true_label, img):
  predictions_array, true_label, img = predictions_array[i], true_label[i], img[i]
  pyplot.grid(False)
  pyplot.xticks([])
  pyplot.yticks([])
  pyplot.imshow(img, cmap=pyplot.cm.binary)

  predicted_label = numpy.argmax(predictions_array)
  if predicted_label == true_label:
    color = 'blue'
  else:
    color = 'red'
  pyplot.xlabel("{} {:2.0f}% ({})".format(class_names[predicted_label],
                                    100*numpy.max(predictions_array),
                                    class_names[true_label]),
                                    color=color)

def plot_value_array(i, predictions_array, true_label):
    predictions, true_label = predictions_array[i], true_label[i]
    pyplot.grid(False)
    pyplot.xticks([])
    pyplot.yticks([])
    thisplot = pyplot.bar(range(10), predictions, color="#777777")
    pyplot.ylim([0, 1])
    predicted_label = numpy.argmax(predictions)

    thisplot[predicted_label].set_color('red')
    thisplot[true_label].set_color('blue')

i = 10
pyplot.figure(figsize=(6,3))
pyplot.subplot(1,2,1)
plot_image(i, predictions, test_labels, test_images)
pyplot.subplot(1,2,2)
plot_value_array(i, predictions, test_labels)
# pyplot.show()

num_rows = 5
num_cols = 3
num_images = num_rows * num_cols
pyplot.figure(figsize=(2 * 2 * num_cols, 2 * num_rows))
for i in range(num_images):
  pyplot.subplot(num_rows, 2 * num_cols, 2 * i + 1)
  plot_image(i, predictions, test_labels, test_images)
  pyplot.subplot(num_rows, 2 * num_cols, 2 * i + 2)
  plot_value_array(i, predictions, test_labels)
# pyplot.show()

img = test_images[0]
print(img.shape)
img = (numpy.expand_dims(img, 0))
print(img.shape)

predictions_single = model.predict(img)
print(predictions_single)
plot_value_array(0, predictions_single, test_labels)
_ = pyplot.xticks(range(10), class_names, rotation=45)
pyplot.show()
print(numpy.argmax(predictions_single[0]))
# pyplot.show()

# Test shape of each layer
conv = keras.layers.Conv2D(3,6,5)(inputs)
print(conv._keras_shape)
model = keres.models.Model(inputs, conv)
model.predict()

print(model.summary)

 x_train_tensor = tensorflow.keras.Input(shape=(32, 32, 3), name='x_train')
 model = tensorflow.keras.layers.Conv2D(6, kernel_size=(5, 5), activation='relu')
 output = model(x_train_tensor)
 output.shape
