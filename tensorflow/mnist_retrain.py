# mnist_retrain.py
import tensorflow
import numpy
import matplotlib.pyplot as pyplot
import utils
import time
from tensorflow.python.tools import inspect_checkpoint as checkpoint
import os
os.environ['TF_CPP_MIN_LOG_LEVEL']='2' # remove warning

def batch_data(X, y, batch_size):
    X_batch = numpy.random.permutation(X)[:batch_size]
    y_batch = numpy.random.permutation(y)[:batch_size]
    Y_batch =to_matrix(y_batch)
    return X_batch, Y_batch

def to_matrix(y):
  N = len(y)
  Y = numpy.zeros((N, 10))
  Y[range(N), y] = 1
  return Y

def next_batch(X, Y, batch_size):

  idx = numpy.arange(0, X.shape[0])
  numpy.random.shuffle(idx)
  idx = idx[:batch_size]
  data_shuffle = [X[i] for i in idx]
  labels_shuffle = [Y[i] for i in idx]
  return numpy.asarray(data_shuffle), numpy.asarray(labels_shuffle)




mnist = tensorflow.keras.datasets.mnist

(X_train, y_train),(X_test, y_test) = mnist.load_data()


N = X_train.shape[0]

X_train = X_train.reshape([N, -1])
Y_train = to_matrix(y_train)

X_test = X_test.reshape(X_test.shape[0], -1)
Y_test = to_matrix(y_test)

print('X train shape: ', X_train.shape)
print('y train shape: ', X_test.shape)


# X_batch, Y_batch = batch_data(X_train, y_train, batch_size)


learning_rate = 0.1
n_epochs = 500
batch_size = 128



D_in, H1, H2, D_out = 784, 256, 256, 10

tensorflow.reset_default_graph()

#************************************ RESTORE MODEL ***************************************


# with tensorflow.Session() as sess:
#   saver.restore(sess, "./model/mnist/mnist_nn.ckpt")
#   print('Model restored.')

#   print('Weight1: ', weights['h1'].eval())
sess = tensorflow.Session()
# saver = tensorflow.train.Saver()
saver = tensorflow.train.import_meta_graph('./model/mnist/mnist_nn.ckpt.meta')
saver.restore(sess, tensorflow.train.latest_checkpoint("./model/mnist/"))
print('Model restored.')
# print('Weight1: ', weights['h1'].eval(sess))

graph = tensorflow.get_default_graph()

w1 = graph.get_tensor_by_name("weight1:0")
w1 = tensorflow.strings.to_number(w1)
# print('Get weight1: ', w1)
# sigma_weight1 = tensorflow.keras.backend.std(weight1.eval(session=sess))
# w1 = weight1 / sigma_weight1
w2 = graph.get_tensor_by_name("weight2:0")
w2 = tensorflow.strings.to_number(w2)


w_out = graph.get_tensor_by_name("weight_out:0")
w_out = tensorflow.strings.to_number(w_out)

b1 = graph.get_tensor_by_name("bias1:0")
b1 = tensorflow.strings.to_number(b1)

b2 = graph.get_tensor_by_name("bias2:0")
b2 = tensorflow.strings.to_number(b2)

b_out = graph.get_tensor_by_name("bias_out:0")
b_out = tensorflow.strings.to_number(b_out)


weights ={
  'h1': w1,
  'h2': w2,
  'out': w_out
}
biases = {
  'b1': b1,
  'b2': b2,
  'out': b_out
}

print('weigth1 shape: ', weights['h1'].shape)
X = tensorflow.placeholder("float", [None, D_in], name='image')
Y = tensorflow.placeholder("float", [None, D_out], name='label')

# weights = {
#     'h1': tensorflow.Variable(tensorflow.random_normal([D_in, H1])),
#     'h2': tensorflow.Variable(tensorflow.random_normal([H1, H2])),
#     'out': tensorflow.Variable(tensorflow.random_normal([H2, D_out]))
# }
# biases = {
#     'b1': tensorflow.Variable(tensorflow.random_normal([H1])),
#     'b2': tensorflow.Variable(tensorflow.random_normal([H2])),
#     'out': tensorflow.Variable(tensorflow.random_normal([D_out]))
# }


layer_1 = tensorflow.add(tensorflow.matmul(X, weights['h1']), biases['b1'])
tensorflow.summary.histogram('weight1', weights['h1'])
tensorflow.summary.histogram('bias1', biases['b1'])
tensorflow.summary.histogram('layer1', layer_1)

layer_2 = tensorflow.add(tensorflow.matmul(layer_1, weights['h2']), biases['b2'])
tensorflow.summary.histogram('weight2', weights['h2'])
tensorflow.summary.histogram('bias2', biases['b2'])
tensorflow.summary.histogram('layer2', layer_2)

logits = tensorflow.matmul(layer_2, weights['out']) + biases['out']
tensorflow.summary.histogram('weight_out', weights['out'])
tensorflow.summary.histogram('bias_out', biases['out'])
tensorflow.summary.histogram('layer_out', logits)


loss_op = tensorflow.reduce_mean(tensorflow.nn.softmax_cross_entropy_with_logits_v2(
    logits=logits, labels=Y))

optimizer = tensorflow.train.AdamOptimizer(learning_rate=learning_rate)

train_op = optimizer.minimize(loss_op)
 

correct_pred = tensorflow.equal(tensorflow.argmax(logits, 1), tensorflow.argmax(Y, 1))
accuracy = tensorflow.reduce_mean(tensorflow.cast(correct_pred, tensorflow.float32))
 

merged = tensorflow.summary.merge_all()
# train_writer = tf.summary.FileWriter(FLAGS.summaries_dir + '/train', sess.graph)


train_writer = tensorflow.summary.FileWriter('./graphs/mnistnn', tensorflow.get_default_graph())
init = tensorflow.global_variables_initializer()



# with tensorflow.Session() as sess:
#   start_time = time.time()
#   sess.run(init)

#   for step in range(n_epochs):
#     batch_x, batch_y = next_batch(X_train, Y_train, batch_size)

#     sess.run(train_op, feed_dict={X: batch_x, Y:batch_y})

#     # loss, acc = sess.run([loss_op, accuracy], feed_dict={X: batch_x, Y:batch_y})
#     loss, acc = sess.run([merged, train_op], feed_dict={X: batch_x, Y:batch_y})
#     train_writer.add_summary(loss, step)

    
#     # print("Step " + str(step) + ", Minibatch Loss= " + \
#     #               "{:.4f}".format(loss) + ", Training Accuracy= " + \
#     #               "{:.3f}".format(acc))
#     # print("Step " + str(step) + ", Training Accuracy= " + \
#     #               "{:.3f}".format(acc))
#     print('Step: ', step)

#   print('Total time: {0} seconds'.format(time.time() - start_time))
#   print('Saving model.............. ')
#   # save_path = saver.save(sess, "./model/mnist/mnist_nn.ckpt")
#   print('Finish save')
#   print("Testing Accuracy:", \
#         sess.run(accuracy, feed_dict={X: X_test,
#                                       Y: Y_test}))

# train_writer.close()
# tensorboard --logdir=./graphs/mnistnn --host 127.0.0.1

