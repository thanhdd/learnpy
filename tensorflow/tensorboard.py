# tensorboard.py
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import numpy
import tensorflow

a = tensorflow.constant(3.0, dtype=tensorflow.float32)
b = tensorflow.constant(4.0) # also tf.float32 implicitly
total = a + b
print(a)
print(b)
print(total)

# writer = tensorflow.summary.FileWriter('.')
# tensorflow.summary.FileWriter('./graphs/lowlelevel', tensorflow.get_default_graph())
writer = tensorflow.summary.FileWriter('./graphs/lowlevel')
writer.add_graph(tensorflow.get_default_graph())
writer.flush()

# tensorboard --logdir=./graphs/lowlevel --host 127.0.0.1


sess = tensorflow.Session()
print(sess.run(total))
print('a, b, total: ', a, b, total)
print(sess.run({'a, b': (a,b), 'total': total}))

vector = tensorflow.random_uniform(shape=(3,))
out1 = vector + 1
out2 = vector + 2
# print(vector, out1, out2)
print(sess.run(vector))
print(sess.run(vector))
print(sess.run((out1, out2)))

x = tensorflow.placeholder(tensorflow.float32)
y = tensorflow.placeholder(tensorflow.float32)
z = x + y

print(sess.run(z, feed_dict={x: 3, y: 4.5}))
print(sess.run(z, feed_dict={x: [1, 3], y: [2, 4]}))

my_data = [[0, 1,], [2, 3,], [4, 5,], [6, 7,],]
slices = tensorflow.data.Dataset.from_tensor_slices(my_data)
next_item = slices.make_one_shot_iterator().get_next()

while True:
  try:
    print(sess.run(next_item))
  except tensorflow.errors.OutOfRangeError:
    break

r = tensorflow.random_normal([10, 3])
dataset = tensorflow.data.Dataset.from_tensor_slices(r)
iterator = dataset.make_initializable_iterator()
next_row = iterator.get_next()

sess.run(iterator.initializer)
while True:
  try:
    print(sess.run(next_row))
  except tensorflow.errors.OutOfRangeError:
    break

x = tensorflow.placeholder(tensorflow.float32, shape=[None, 3])
linear_model = tensorflow.layers.Dense(units=1)
y = linear_model(x)

# Initializing Layers
init = tensorflow.global_variables_initializer()
sess.run(init)

# Executing Layers
print(sess.run(y, {x: [[1, 2, 3], [4, 5, 6]]}))
