# training_classifier.py
import torch
import torchvision
import torchvision.transforms as transforms
import matplotlib.pyplot as pyplot
import numpy
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim

from torch.autograd import Variable

class Net(nn.Module):
  def __init__(self):
    #     torch.nn.Conv2d(input_channel,output_channel,filter_size,stride) # 
    # # Example:
    # # image: 32x32x3, 3 is input channels
    # # input = 50000 x 3 x 32 x 32 # N = 50000, input_channel = 3 
    # # model = torch.nn.Conv2d(3, 6, 5) # input channels = 3, output channels = 6, filter size = 5x5, stride = 1
    # # output = 50000 x 6 x 28 x 28 
    super(Net, self).__init__()
    input_channels = 3 # 
    output_channels = 6 #kernel size or filters
    filter_size = 5 # 5 x 5
    self.conv1 = nn.Conv2d(input_channels, output_channels, filter_size)

    self.pool = nn.MaxPool2d(2, 2)

    input_channels = 6
    output_channels = 16
    self.conv2 = nn.Conv2d(input_channels, output_channels, filter_size)


    self.fc1 = nn.Linear(16 * 5 * 5, 120)
    self.fc2 = nn.Linear(120, 84)
    self.fc3 = nn.Linear(84, 10)

  def forward(self, x):
    x = self.pool(F.relu(self.conv1(x)))
    x = self.pool(F.relu(self.conv2(x)))
    x = x.view(-1, 16 * 5 * 5)
    x = F.relu(self.fc1(x))
    x = F.relu(self.fc2(x))
    x = self.fc3(x)
    return x


def imshow(image):
  image = image / 2 + 0.5
  numpy_image = image.numpy()
  pyplot.imshow(numpy.transpose(numpy_image, (1,2,0)))
  pyplot.show()


transform = transforms.Compose(
                              [transforms.ToTensor(),
                                transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5))])

trainset = torchvision.datasets.CIFAR10(root='./data', train=True, download=True, transform=transform)

trainloader = torch.utils.data.DataLoader(trainset, batch_size=4, shuffle=True)

test_set = torchvision.datasets.CIFAR10(root='./data', train=False, download=True, transform=transform)

testloader = torch.utils.data.DataLoader(test_set, batch_size=4, shuffle=False)

classes = ('plane', 'car', 'bird', 'cat', 'deer', 'dog', 'frog', 'horse', 'ship', 'truck')

# data_iter = iter(trainloader)
# images, labels = data_iter.next()

# #show image
# # imshow(torchvision.utils.make_grid(images))

# #print labels
# print(' '.join('%5s' % classes[labels[j]] for j in range(4)))


#Convolution: 
# 
net = Net()
criterion = nn.CrossEntropyLoss()
optimizer = optim.SGD(net.parameters(), lr=0.001, momentum=0.9)
net.cuda()

for epoch in range(2):
  running_loss = 0.0
  """
  for i, data in enumerate(trainloader, 0):
    inputs, labels = data
  """
  for i, (inputs, labels) in enumerate(trainloader):
    inputs = Variable(inputs).cuda()
    labels = Variable(labels).cuda()
    optimizer.zero_grad()

    outputs = net(inputs)
    loss = criterion(outputs, labels)
    loss.backward()
    optimizer.step()

#running_loss += loss.item()
    if i % 2000 == 1999:
      print('[%d, %5d] loss: %0.3f' %(epoch + 1, i + 1, oss / 2000))
      running_loss = 0.0

print('Finished Training')

data_iter = iter(testloader)
images, labels = data_iter.next()
imshow(torchvision.utils.make_grid(images))
print(' '.join('%5s' % classes[labels[j]] for j in range(4)))

