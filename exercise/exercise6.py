#    Lab 6

# This lab assumes that you have finished the CNN tutorial for MNIST.
# Get the fully-working tutorial ready. We are going to modify the 4-layer CNN for CIFAR-10
# and in doing so, we trouble-shoot the issues being encountered.


# 1.  Let’s prepare the data first. In the CIFAR-10, the labels are formatted differently.
#  Let’s convert it into the format we used for the MNIST using numpy. 
# (Hints: use indexing). Check if your conversion is right before proceed.

# 2.  Concatenate all 5 data_batch files into a single array.

import numpy
import _pickle as cPickle
import tensorflow
from tensorflow import keras
import matplotlib.pyplot as pyplot

def unpickle(file):   
    opened_file = open(file, 'rb')     
    dict = cPickle.load(opened_file, encoding='latin1')    
    opened_file.close()    
    return dict

def get_train_data():
    data_dir = 'cifar-10-batches-py/data_batch_'
    batch_data = [dict() for i in range(5)]
    batch_num = 5
    
    for i in range(batch_num):
      batch_data[i] = unpickle(data_dir + str(i + 1))
      if i == 0:       
        train_data = batch_data[i]['data'] # 10000x3072
        train_labels = batch_data[i]['labels'] # 1x10000
      else:
        train_data = numpy.append(train_data, batch_data[i]['data'],axis=0)
        train_labels = numpy.append(train_labels, batch_data[i]['labels'], axis=0)
    train_data = train_data.T # 3072x50000
    return train_data, train_labels

def get_test_data():
    test_batch_data = unpickle('cifar-10-batches-py/test_batch')
    test_data = test_batch_data['data'] # 10000x3072
    test_data = test_data.T
    test_labels = numpy.array(test_batch_data['labels'])
    
    return test_data, test_labels

X, y = get_train_data()

print('X shape: ', X.shape)
print('y shape: ', y.shape)

# 3.  We are going to replace “batch = mnist.train.next(50)” by a new code.
# Write a piece of codes that slice the training images and the format-converted labels for mini-batch SGD.
cifar10 = keras.datasets.cifar10
(train_data, train_labels), (test_data, test_labels) = cifar10.load_data()
class_names = ['plane', 'car', 'bird', 'cat', 'deer', 'dog', 'frog', 'horse', 'ship', 'truck']
print(train_data.shape)
print(train_labels.shape)
print(test_data.shape)
print(test_labels.shape)

pyplot.figure()
pyplot.imshow(train_data[0])
pyplot.colorbar()
# pyplot.show()

pyplot.figure(figsize=(10,10))
for i in range(25):
  pyplot.subplot(5,5,i+1)
  pyplot.xticks([])
  pyplot.yticks([])
  pyplot.grid(False)
  pyplot.imshow(train_data[i], cmap=pyplot.cm.binary)
  # train_labels = numpy.squeeze(train_labels)
  # print(train_labels.shape)
  pyplot.xlabel(class_names[train_labels[i][0]])
# pyplot.show()

# 4.  Let’s re-architecture the network for 32x32x3 inputs. 
# Calculate the shape of the output volumes of each layer. 
# Then adjust the shape of the first fully connected layer.

model = keras.models.Sequential()
model.add(keras.layers.Conv2D(6, kernel_size=(5,5), activation='relu', input_shape=(32,32,3)))
print('Model summary: ', model.summary)
model.add(keras.layers.MaxPooling2D(pool_size=(2,2)))
model.add(keras.layers.Conv2D(16, kernel_size=(5,5), activation='relu', input_shape=(14,14,6)))
# model.add(keras.layers.MaxPooling2D(pool_size=(2,2)))
model.add(keras.layers.Flatten())
model.add(keras.layers.Dense(120, activation='relu'))
model.add(keras.layers.Dense(84, activation='relu'))
model.add(keras.layers.Dense(10))
         
# 5.  You may encounter “NaN” during training. Why is Nan? How to deal with NaN during training? 
# 1) Decrease the learning rate
# model.compile(optimizer=tensorflow.train.AdamOptimizer(0.0001),
#               loss='sparse_categorical_crossentropy',
#               metrics=['accuracy'])
model.compile(optimizer=tensorflow.train.AdamOptimizer(0.01),
              loss='sparse_categorical_crossentropy',
              metrics=['accuracy']

model.fit(train_data, train_labels, epochs=1)
test_loss, test_accuracy = model.evaluate(test_data, test_labels)
print('Test loss: ', test_loss)
print('Test accuracy: ', test_accuracy)
print('Model summary: ', model.summary)



. 
# 2) Check the initialization. 
# kernel_initializer='glorot_uniform'
# 6.  Draw the distributions of activations and weights using TensorBoard.












# 7.  Vary the hyperparameters for the initialization and check how the distribution changes.


# 8.  Plot the training accuracy and test accuracy over time using TensorBoard.


# 9.  Vary the learning rate and check how the plot changes.


# 10. Use weight decay to prevent overfitting.
