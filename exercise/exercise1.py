# import math

A = ["we", "are", "at", "inu"]
print A
# 1.  Obtain the index of  "at" in the list.'
print 'Index of "at": ', A.index("at")
# 2.  Insert "all" right after "we".'
A.insert(A.index("we") + 1, "all")
print A
# 3.  Append "." at the end.'
A.append(".")
print A
# 4.  Concatenate the strings in A.'
length = len(A)
string = ' '.join(A[0:length-1]) + A[length-1]
print string
# 5.  Delete "at" and "inu" from the list.'
A.remove("at")
print A
# 6.  Iterate over all items in list A.'
for word in A:
  print word
# 7.  While iterating over all items in list A, print the index and the item together.'
for i in range(len(A)):
  print i, A[i]

for i, v in enumerate(A):
  print i, v

A = {"Math" : "90", "English" : "30", "Computer" : "100"}
print A
# 1.  Calculate the total of all the scores.'
print sum(map(lambda x: int(x), A.values()))
print sum(int(score) for score in A.values())

# 2.  Add a new subject and its score.
A["Programming Language"] = "100"
print A

# 3.  Remove  "Math".
del A['Math']
print A

# 4.  Iterate over all items and print the key-value pair.
for k,v in A.items(): # [('Computer', '100'), ('Math', '90'), ('English', '30')]
  print k, v

for k,v in A.iteritems(): # use Iterator
  print k, v

# 5.  Obtain the list of the keys.
print A.keys()

# 6.  Obtain the list of the values.
print A.values()

# 7.  Check if the dict A has "Science" key.
print "Science" in A

# Objected-oriented Programming'
# Object (Class) = a group of variables + associated functions
# Organizes data and functions using hierarchy (factoring)
class Member(object):
  def __init__(self,name, id):
    self.name = name
    self.id = id
  
  def description(self):
    return self.name + ": " + self.id

# A professor has a "name", a "id".'
class Professor(Member):
  pass

# A student has a "name", a "id", pairs of "course taken" and "grades".
class Student(Member):
  def __init__(self,name,id):
    super(Student, self).__init__(name,id)
    self.transcript = {}

# Write a function that prints the id and the name of each member of the university.
def print_all(members):
  for mem in members:
    print mem.description()

# Write a function that prints the course taken and the grades
def print_transcript(student):
  for course, grade in student.transcript.iteritems():
    print course + ": " + str(grade)

members = []
prof = Professor("Professor A","100")
members.append(prof)

student1 = Student('Studen A', '101')
student1.transcript['Math'] = 90
student1.transcript['English'] = 95
members.append(student1)

student2 = Student('Student B', '102')
student2.transcript['Programming'] = 100
student2.transcript['English'] = 95
members.append(student2)

print_all(members)

print_transcript(student1)
print_transcript(student2)
