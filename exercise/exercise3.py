# # exercise3.py
# Lab 3

# 1.  Download the CIFAR-10 dataset: https://www.cs.toronto.edu/~kriz/cifar-10-python.tar.gz

 
# 2.  Write a simple python program that loads the data set and displays the first 10 images in the first batch one at a time.
 # Refer to http://www.cs.toronto.edu/~kriz/cifar.html. 

# The archive contains the files data_batch_1, data_batch_2, ..., data_batch_5, as well as test_batch.
# Each of these files is a Python "pickled" object produced with cPickle. Here is a Python routine which will open such a file and return a dictionary:
# Loaded in this way, each of the batch files contains a dictionary with the following elements:
# data -- a 10000x3072 numpy array of uint8s. Each row of the array stores a 32x32 colour image.
# The first 1024 entries contain the red channel values, the next 1024 the green, and the final 1024 the blue.
# The image is stored in row-major order, so that the first 32 entries of the array are the red channel values of the first row of the image.
# labels -- a list of 10000 numbers in the range 0-9. The number at index i indicates the label of the ith image in the array data.

import numpy
import matplotlib.pyplot as pyplot
import time

def unpickle(file):   
  import cPickle     
  opened_file = open(file, 'rb')     
  dict = cPickle.load(opened_file)    
  opened_file.close()    
  return dict

batch_data = unpickle('cifar-10-batches-py/data_batch_1')
data = batch_data['data'] # 10000x3072
image_num = data.shape[0] # 10000

# Solution 2.

def display_image(data):
  rows, columns = 1, 10
  width, height = 10, 3
  figure, axes = pyplot.subplots(rows,columns,figsize=(width,height))
  image_num = 10
  for i in range(image_num):
    image = data[i,:] #each row is an image
    image = numpy.transpose(image.reshape(3,32,32),(1,2,0))
    axes[i].imshow(image)
  pyplot.show()

display_image(data)




# 3.  Write the nearest neighbor classifier for the CIFAR10 task.
# a)  Get familiar with the following broadcasting rules:

# ** Broadcasting rules: 

# When operating on two arrays, NumPy compares their shapes element-wise.
# It starts with the trailing dimensions, and works its way forward.
# Two dimensions are compatible when
# 1.  they are equal, or
# 2.  one of them is 1
# Arrays do not need to have the same number of dimensions

class NearestNeighborClassifier(object):
  def __init__(self):
    pass

  def train(self, X, y):
    self.X_train = X
    self.y_train = y

  def predict(self, X):
    num_test = X.shape[0]
    Y_predict = numpy.zeros(num_test, dtype = self.y_train.dtype)

    for i in range(num_test):
      norm = 1

      start = time.time()
      distances = numpy.sum(abs(self.X_train - X[i,:]), axis = 1)
      # distances = numpy.linalg.norm(self.X_train - X[i,:],norm,axis = 1)
      end = time.time()
      print 'Norm benchmark: ', end - start

      neighbors = numpy.argsort(distances)[0:1]
      min_index = numpy.argmin(distances)
      Y_predict[i] = self.y_train[min_index]

    return Y_predict

batch_data = unpickle('cifar-10-batches-py/data_batch_3')
X_train = batch_data['data'] # 10000x3072
# print X_train.shape
y_train = numpy.array(batch_data['labels'])

nearest_neightbor = NearestNeighborClassifier()
nearest_neightbor.train(X_train, y_train)

# b)  Obtain the distance vector using L1 norm between 10000 training images each represented in a 3072-dimensional vector 
# and the first image in the test set represented in a 3072-dimensional vector.
test_data = unpickle('cifar-10-batches-py/test_batch')
x_test = test_data['data']
y_test = test_data['labels']

X = test_data['data'][0:1,:]

# c)  Find the index of the training image with the minimum distance.
print 'Nearest Neighbor: '
print 'Benchmark : '
start = time.time()

Y_predict = nearest_neightbor.predict(X)

end = time.time()

print end - start

print 'Y predict: ', Y_predict
# d)  Find the label of the training image.
print 'Y label: ', y_test[0]

# e)  Loop over all test images and calculate the prediction accuracy of the nearest neighbor

""" Solution 3.e

print "Benchmark: "
start = time.time()

y_predict = nearest_neightbor.predict(x_test)

end = time.time()
print end - start # 883 second on neptunecloud,4 cpus, 15GB RAM

accuracy = numpy.mean(y_predict == y_test)
print 'Accuracy: ', accuracy #21.76%

"""

# 4.  Write the K-nearest neighbor classifier for the CIFAR10 task.
class KNNClassifier(object):

  def __init__(self):
    pass

  def train(self, X, y):
    self.X_train = X
    self.y_train = y

  def predict(self, X, k=1, norm=1):
    if k > self.X_train.shape[0]:
      print 'k has to lower or equal number of training samples'
      return

    num_test = X.shape[0]
    Y_predict = numpy.zeros(num_test, dtype = self.y_train.dtype)

    for i in range(num_test):
      if norm == 2:
        distances = numpy.sqrt(numpy.sum(numpy.square(self.X_train - X[i,:]), axis = 1))
      else:
        start = time.time()
        # distances = numpy.sum(abs(self.X_train - X[i,:]), axis = 1)    
        distances = numpy.linalg.norm(self.X_train - X[i,:],norm,axis = 1)#- BAD PERFORMANCE 5 TIMES TO ABOVE COMMAND
        end = time.time()
        print 'Norm benchmark: ', end - start

      k_neighbors = numpy.argsort(distances)[0:k]
      neighbor_labels = self.y_train[k_neighbors]
      Y_predict[i] = self.major_vote(neighbor_labels)

    return Y_predict

  def major_vote(self,votes):
    vote_labels, vote_indices, vote_counts = numpy.unique(votes, True, False, True)
    major_vote_index = numpy.argsort(vote_counts)[-1]      
    return vote_labels[major_vote_index]

print "KNN Classifier: "
knn_classifier = KNNClassifier()
knn_classifier.train(X_train, y_train)

# k = 1, norm = 1
print 'Benchmark kNN: '
start = time.time()

Y_predict = knn_classifier.predict(X,1,1)

end = time.time()
print end - start

print 'Y predict: ', Y_predict

print 'Y label: ', y_test[0]

# Run KNN with variations of K

ks = [1,2,3,4,5,6,7,8,9,10]
norms = [1,2]
accuracies_l1 = []
accuracies_l2 = []
times = []
# ''' Solution 4'''
# for k in ks:
#   print 'k = ', k
#   # print "Benchmark: "
#   start = time.time()

#   y_predict = knn_classifier.predict(x_test,k,norms[0])

#   end = time.time()
#   runtime = end - start
#   print "Runtime: ", runtime 

#   accuracy = numpy.mean(y_predict == y_test)
#   print 'Accuracy: ', accuracy 
#   accuracies_l1.append(accuracy)
#   times.append(runtime)

# print ks
# print accuracies_l1
# print times
# batch1 
accuracies_l1 = [0.21759999999999999, 0.2293, 0.23230000000000001, 0.23000000000000001, 0.23150000000000001, 0.2374, 0.2341, 0.23380000000000001, 0.2311, 0.23230000000000001]
# batch2 accuracies_l1 = [0.2235,0.2376,0.2375,0.2359,0.2361,0.2373,0.237,0.2388,0.2397,0.2406]
# batch3 accuracies_l1 = [0.2193, 0.2336, 0.2327, 0.2313, 0.2336, 0.237, 0.2341, 0.2323, 0.2329, 0.2304]
# batch4 accuracies_l1 = [0.2265, 0.2375, 0.237, 0.2393, 0.2399, 0.2414, 0.244, 0.2412, 0.2378, 0.2366]
accuracies_l2 = [0.19850000000000001, 0.2074, 0.20960000000000001, 0.2087, 0.21129999999999999, 0.21390000000000001, 0.2213, 0.22090000000000001, 0.21970000000000001, 0.21840000000000001]
# 5.  Plot the accuracy varying the K value.

pyplot.plot(ks, accuracies_l1)
pyplot.plot(ks, accuracies_l2)
pyplot.xlabel('K')
pyplot.ylabel('Accuracy')
pyplot.title('Accuracy by K value - K Nearest Neighbor')
pyplot.legend(['L1', 'L2'])
pyplot.show()

# 6. Set up the latest PyTorch on your machine according to the course website. => Done

