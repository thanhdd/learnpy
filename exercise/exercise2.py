# Lab 2
# Import numpy as np
import numpy
# A 1st order tensor (i.e., vector) is given as follows:
A = numpy.array([1, 2, 4, 5, 8, 9])
print A
# Folding/Unfolding
# 1.  Fold (i.e., matricize) the vector A into a 2x3 matrix.
A = A.reshape(2,3)
print A
# 2.  Unfold (i.e., flatten) the 2x3 matrix into the original vector
A = A.flat
print A[:]
# Change axes(i.e., modes)
# 3.  Swap the two axes (i.e., modes). (hint : Transpose)
print numpy.transpose(A)
# 4.  Permutates the axes of a 4-th order tensor.
tensor = numpy.arange(16).reshape(2,2,2,2)
print tensor
print numpy.transpose(tensor, (3,2,1,0))
# Norms
# 5.  Calculate the average of A.
print numpy.average(A)
print numpy.mean(A)
# 6.  Calculate the variance and standard deviation of A.
standard_deviation = numpy.std(A) # root of sum of square of A - mean
print standard_deviation
variance_deviation = numpy.var(A) # square
print variance_deviation
# 7.  What is the L1 norm of A? sum of element abs
l1_norm = numpy.linalg.norm(A,1)
# 8.  What is the L2 norm of A? root of sum of element square
l2_norm = numpy.linalg.norm(A)
print l2_norm
# 9.  What is the Frobenius norm of the 2x3 matrix - l2 norm
print l2_norm
# Arithmetic Operations
# 10. Multiply the 2x3 matrix by a vector
C = numpy.arange(6).reshape(2,3)

vector = numpy.arange(3).reshape(3,1)
print C.dot(vector)
# 11. Multiply the 2x3 matrix by a matrix elementwise.
B = numpy.arange(6).reshape((2,3))
print C * B

# Indexing
# 12. Obtain the array of the indices of the elements that are greater than 5 in A.
print numpy.argwhere(A > 5)


# 13. Obtain the array of the elements that are greater than 5 in A.
print A[A > 5]
# Sorting
# 14. Obtain the top 5 largest elements in A.
print numpy.sort(A)[-5:]
# 15. Obtain the indices of the top-5 largest elements in A.
print numpy.argsort(A)[-5:]
# Slicing/Concatenation
# 16. Practice slicing/concatenation functions

# Array Creation
# 17. Create a 4th order tensor where each element is drawn from a Gaussian distribution N(0,1) (mean, standard, size)
T = numpy.random.normal(0,1,16).reshape(2,2,2,2)
# 18. Create a random matrix where each element is drawn from a uniform distribution 1/b-a ( low, high, size)
U = numpy.random.uniform(0,1,5)
# 19. Create a zero matrix.
zeros = numpy.zeros((2,3))

# Manipulating Images (i.e., a matrix of pixels)
import scipy
import imageio
import matplotlib.pyplot as pyplot
import matplotlib.image as mpimg
# from PIL import Image

# # 20. Read an image using imread
# imageio.imread('chelsea.png')

# 21. Resize it into 1/4 size.


# 22. Perform a crop

# Ploting
# 23. Plot the A vs the index graph.
pyplot.plot(A)
pyplot.ylabel('A')
pyplot.show()
