

# exercise4.py
#    Lab 4

# Pytorch is a Python based scientific computing package targeted at two sets of audiences:
# 1.  A replacement for numpy to use the power of GPUs
# 2.  a deep learning research platform that provides maximum flexibility and speed 
# Walk through the following tutorials:
# http://pytorch.org/tutorials/beginner/deep_learning_60min_blitz.html

# BETTER TUTORIALS
# https://pytorch.org/tutorials/beginner/pytorch_with_examples.html
#https://towardsdatascience.com/getting-started-with-pytorch-part-1-understanding-how-automatic-differentiation-works-5008282073ec

from __future__ import print_function
import torch
import time
import sys
import py3nvml.nvidia_smi as smi
import matplotlib.pyplot as pyplot
import numpy

# 1.  Create a tensor in CPU memory and transfer it to GPU memory.
 # Measure the transfer time with respect to the number of the size of the tensor.
# Plot the graph and describe your hardware setting including which PCI slot your gpu is using (e.g., 16x, 8x, 4x, etc)
tensor1 = torch.rand(100,100)
tensor2 = torch.rand(100,100,100)
tensor3 = torch.rand(100,100,100,100)

if not torch.cuda.is_available():
	sys.exit()

# device = torch.device("cuda")

# start = time.time()
# tensor1 = tensor1.to(device)

# time1 = time.time()
# tensor2 = tensor2.to(device)

# time2 = time.time()
# transfer_time2 = time2 - time1

# tensor3 = tensor3.to(device)
# time3 = time.time()

# transfer_time1 = time1 - start
# print('Transfer time 1: ', transfer_time1)
# transfer_time2 = time2 - time1
# print('Transfer time 2: ',transfer_time2)
# transfer_time3 = time3 - time2
# print('Transfer time 3: ', transfer_time3)

x = torch.rand(5)
y = torch.rand(5)


pyplot.plot([1,2,3], [4,5,6])

x = torch.ones(1200)
y = torch.ones(1200,1,1,1,1)
# print(x)
# print(y)
x.size()
torch.Size([1200])


y.size()
torch.Size([1200, 1, 1, 1, 1])

y.squeeze().size()
torch.Size([1200])

pyplot.plot(x.numpy(), y.squeeze().numpy())

# pyplot.show()

# print(smi.XmlDeviceQuery())


# 2.  Create two random matrices directly in CPU and multiply them in CPU. 
# Repeat it for GPU. Measure the computation time varying the size of the matrices. 
A = torch.rand(10000, 10000)
B = torch.rand(10000, 10000)

# start = time.time()
# C =  A @ B 
# end = time.time()
# cpu_time = end  - start
# print("CPU Time: ", cpu_time) # 5.78369 s

# A = A.to(device)
# B = B.to(device)
# start = time.time()
# D =  A.to(device) @ B.to(device)
# end = time.time()
# gpu_time = end  - start
# print("GPU Time", gpu_time) # 0.39065 s


# 3.  A=A+B . Perform this operation using the in-place op. Compare the runtime to  that of not using the in-place op.  

start = time.time()
A = A + B
out_place_time = time.time() - start

start = time.time()
A = A.add_(B)
in_place_time = time.time() - start
print("out place time", out_place_time)
print("in place time", in_place_time)

# 4.  Evaluate X^2 + aX + b  when X = 1 and a=-10 and b=25 using Variable. 

# VARIABLE IS DEPRECATED

X = torch.tensor((1.), requires_grad=True)
a = torch.tensor((-10.), requires_grad=True)
b = torch.tensor((25.), requires_grad=True)

y = X*X + a*X + b
out = y.mean()
out.backward()


# 5.  Compute the gradients of the above function with respect to X, a, b when X=1, a=-10, and b=25.
# Check the results with your hand-calculated ones.
X.grad # -8
a.grad # 1
b.grad # 1


# 6.  Create a softmax classifier for CIFAR-10. Initialize the weights are small so that all the scores are close to 0. 
# Check if the initial loss is close to log 10. 
class MulticlassSVM(object):
  def __init__(self):
    pass

  def loss_i_half_vectorized(x,y,W):
    delta = 1.0
    scores = W @ x
    margins = numpy.maximum(0, scores - scores[y] + delta)
    margins[y] = 0
    loss_i = numpy.sum(margins)
    return loss_i

  def loss(self,X, y, W):
    delta = 1.0
    N = X.shape[1]

    scores = W @ X  
    correct_scores = scores[y, range(N)]
    margins = numpy.maximum(0, scores - correct_scores + delta)
    loss = (numpy.sum(margins) - N * delta)/N
    return loss

  def gradient_descent(self, X, y, W)
    delta = 1.0
    N = X.shape[1]
    scores = W @ X  
    correct_scores = scores[y, range(N)]
    margins = numpy.maximum(0, scores - correct_scores + delta)                                            
    margins[y, range(N)] = 0

    temp = (margins > 0).astype(int)
    temp[y, range(N)] = numpy.sum(-temp, axis=0)
    grad = X @ temp.T / N
  return grad

    # correct_scores = numpy.choose(y, scores)
    # loss = numpy.sum(margins)/N
    
class SoftmaxClassifier(object):
  def __init__(self):
    pass

  def loss(self,X, y, W):    
    scores = W @ X
    correct_scores = numpy.choose(y, scores)
    max_scores = numpy.max(scores, axis=0)
    scores -= max_scores # easy for numeric computation

    loss = (-numpy.sum(correct_scores) + numpy.sum(numpy.log(numpy.sum(numpy.exp(scores), axis=0)))) / N

    return loss

  def gradient_descent(self, X, y, W, learning_rate, num_iters):
    scores = W @ X
    N = X.shape[1]
    loss_history = numpy.zeros((num_iters))

    correct_scores = numpy.choose(y, scores)

    grad = (scores - correct_scores) @ X.T / N

    for iter in range(num_iters):
      W -= learning_rate * grad

      loss_history[iter] = self.loss(X, y, W)

    return W, loss_history

  def sample_training_data(X, y, batch_size):
    X_batch = numpy.random.permutation(X)[:batch_size]
    y_batch = numpy.random.permutation(y)[:batch_size]
    return X_batch, y_batch

  def stochastic_gradient_descent(self, X, y, W, learning_rate, num_iters, batch_size):
    X_batch, y_batch = sample_training_data(X,y,batch_size)
    return gradient_descent(X_batch, y_batch, W, learning_rate, num_iters)

import cifar

def random_search_weight(f,X, y):
  best_loss = float("inf")
  for num in range(1000):
    W = numpy.random.randn(10,3073) * 0.0001
    loss = f(X, y, W)
    if loss < best_loss:
      best_loss = loss
      bestW = W
    print('in atempt %d the loss was %f, best %f ' %(num, loss, best_loss)) # 
  
  return bestW

def check_accuracy(W):
  X_test, y_test = cifar.get_test_data() # 3072x10000
  X_test = numpy.append(X_test, numpy.ones((1,10000)), axis=0) # add bias

  scores = W @ X_test
  y_test_predict = numpy.argmax(scores, axis=0)
  return numpy.mean(y_test_predict == y_test)


X, y = cifar.get_training_data()
K = 10
N = X.shape[1] # 50000
D = X.shape[0] # 3072

X = numpy.append(X, numpy.ones((1,N)), axis=0) # Add bias
W = numpy.full((K, D + 1),1.0)

learning_rate = 1e-9
print('learning_rate: ', learning_rate)
num_iters = 100

print('W shape: ', W.shape)
print('X shape: ', X.shape)
print('y shape: ', y.shape)


svm_classifier = MulticlassSVM() 
svm_loss = svm_classifier.loss(X, y, W)
print('SVM Loss: ', svm_loss)

# Random search for SVM 
print('SVM Random search')
# svm_random_weight = random_search_weight(svm_classifier.loss,X,y)
# numpy.savetxt('svm_random_weight.out', svm_random_weight)
# print('SVM Random search accuracy: ', check_accuracy(svm_random_weight))

# Random search for Softmax 
print('Softmax Random search')
softmax_classifier = SoftmaxClassifier()
# softmax_random_weight = random_search_weight(softmax_classifier.loss,X,y)
# numpy.savetxt('softmax_random_weight.out', softmax_random_weight)
# print('Random search accuracy: ', check_accuracy(softmax_random_weight))


# 7.  Minimize the loss with respect to the weights using the gradient descent.   (Do not use optim package!).
def autograd_softmax(X,y,W, learning_rate, num_iters):
  K = W.shape[0] #10
  N = X.shape[1] #50 000
  # device = torch.device("cuda")
  X_tensor = torch.from_numpy(X)
  y_tensor = torch.from_numpy(y)
  W_tensor = torch.from_numpy(W).requires_grad_(True)
  lost_history = torch.zeros((num_iters))
  correct_scores = torch.zeros(N)
  indices = numpy.zeros(N, dtype=numpy.long)
  # print('N: ', N)
  for i in range(N):
    indices[i] = y[i] * N + i

  indices = torch.tensor(indices, dtype=torch.long)

  for iter in range(num_iters):  
    scores = W_tensor @ X_tensor
    correct_scores = torch.take(scores, indices)


    # print('Correct Scores: ', correct_scores)
    max_scores, max_indices = torch.max(scores, dim=0)
    scores -= max_scores
    loss = (-torch.sum(correct_scores) + torch.sum(torch.log(torch.sum(torch.exp(scores), dim=0)))) / N
    loss.backward()
    print('Softmax Autograd iter: %d loss: %f', (iter, loss.item()))
    lost_history[iter] = loss.item()

    with torch.no_grad():
      W_tensor -= learning_rate * W_tensor.grad

      W_tensor.grad.zero_()

  return W_tensor, lost_history


W = numpy.loadtxt('softmax_random_weight.out')

# W, loss_history = softmax_classifier.gradient_descent(X, y, W, learning_rate, num_iters)
# numpy.savetxt('gradient_descent_weight', W)
# print('Gradient Descent Lost history: ', loss_history)
# print('Gradient Descent accuracy: ', check_accuracy(W))

# W_tensor, loss_history = autograd_softmax(X, y, W, learning_rate, num_iters)
# W = W_tensor.detach().numpy()
# numpy.savetxt('autograd_softmax_weight', W)
# print('AutoGrad Softmax accuracy: ', check_accuracy(W))



# 8.  Minimize the loss with respect to the weights using the stochastic gradient descent with batch size = 256.  
# Implement the two versions of it, one of which uses optim and the other doesn’t use it. Compare the two versions.
batch_size = 256
# W = numpy.loadtxt('softmax_random_weight.out')
# W, loss_history = softmax_classifier.stochastic_gradient_descent(X, y, W, learning_rate, num_iters)
# numpy.savetxt('stochastic_gradient_descent_weight', W)
# print('Stochastic Gradient Descent Lost history: ', loss_history)
# print('Stochastic Gradient Descent accuracy: ', check_accuracy(W))

def sgd_optim(X, y, W, learning_rate, num_iters, batch_size):

  K = W.shape[0] #10
  N = X.shape[1] #50 000
  # device = torch.device("cuda")
  N = batch_size
  batch_indices = torch.randperm(N)[0:batch_size]

  X_batch = torch.index_select(torch.from_numpy(X), 1, batch_indices)

  y_batch = torch.index_select(torch.from_numpy(y), 0, batch_indices)

  W_sgd = torch.from_numpy(W).requires_grad_(True)

  lost_history = torch.zeros((num_iters))

  optimizer = torch.optim.SGD([W_sgd], learning_rate)
  for iter in range(num_iters):  
 
    optimizer.zero_grad()
    scores = W_sgd @ X_batch
    correct_scores = scores[y[0:batch_size], range(N)]

    max_scores, max_indices = torch.max(scores, dim=0)
    scores -= max_scores
    loss = (-torch.sum(correct_scores) + torch.sum(torch.log(torch.sum(torch.exp(scores), dim=0)))) / N
    loss.backward()

    print('SGD Optim iter: %d loss: %f', (iter, loss.item()))
    lost_history[iter] = loss.item()

    optimizer.step()

  return W_sgd, lost_history


W_sgd, loss_sgd = sgd_optim(X, y, W, learning_rate, num_iters, batch_size)
W = W_sgd.detach().numpy()
numpy.savetxt('sgd_optim_weight', W)
print('SGD Optim accuracy: ', check_accuracy(W))