# exercise5.py
#    Lab 5
import torch
import numpy
import matplotlib.pyplot as pyplot
import cifar
# 1.	Load one batch of the cifar-10 training data. Take first 100 samples, and we will use only two features out of the 512 features (i.e., pixels).
#  Then you should have the 100x2 data matrix D. 

X, y = cifar.get_training_data()
print(X.T.shape)
X_batch = X.T[0:100,0:2]
y_sample = y[0:100]
print(X_batch.shape)

print('Show data')
pyplot.scatter(X_batch[:, 0], X_batch[:, 1])
pyplot.xlabel('X0')
pyplot.ylabel('X1')
# pyplot.show()
# 2.	Subtract the mean and normalize the data.
X_batch = X_batch.astype(numpy.float64)
X_batch -= numpy.mean(X_batch, axis=0)
print('Show mean')
pyplot.scatter(X_batch[:, 0], X_batch[:, 1])
# pyplot.show()

X_batch /= numpy.std(X_batch, axis=0)

# 3.	Plot the scatter plot. Examine how much the two features are correlated. Linear function
print('Show standard')
pyplot.scatter(X_batch[:, 0], X_batch[:, 1])
# pyplot.show()

# 4.	Calculate the covariance matrix K.  What are the diagonal elements?  Is it symmetric? YES
K = numpy.dot(X_batch.T, X_batch)/X_batch.shape[0] 
print('X batch: ',X_batch)
print(K)
print('K shape: ', K.shape, K)
# coveriance = numpy.cov(X_batch.T)
# print('Coveriance: ', coveriance)

# 5.	Use the eigenvector matrix of the covariance matrix as the transform matrix.
# Project the data into the new basis. Plot the scatter plot. Let T be the projected data. 
eigenvalue, eigenvector = numpy.linalg.eig(K)
print('Eigenvalue: ', eigenvalue)
print('Eigenvector: ', eigenvector)
T = X_batch @ eigenvector[:,0:1]


# 6.	Recover the original data from T using the inverse transform matrix

X_recover = T @ eigenvector[:,0:1].T

# 7.	Perform the singular value decomposition. Then we have D = USV.  Use VT as the transform matrix and repeat 5-6.  

u,s,v = numpy.linalg.svd(K.T)
VT = X_batch @ u[:,0:1]
X_rec = VT @ u[:,0:1].T

# 8. Repeat 3-7 in PyTorch.
# 4.
X_tensor = torch.from_numpy(X_batch)
print('X tensor: ', X_tensor)
K_tensor = X_tensor.t() @ X_tensor / X_tensor.shape[0]
print('K_tensor: ', K_tensor)
# 5.
eig_value, eig_vector = torch.eig(K_tensor, True)
print('Eigenvalue: ', eig_value)
print('Eigenvector: ', eig_vector)
T_tensor = X_tensor @ eig_vector[:,0:1]
print('T_tensor: ', T_tensor)
X_tensor_recover = T_tensor @ eig_vector[:,0:1].t()
u2,s2,v2 = torch.svd(K_tensor)
print('u2, s2 , v2', u2, s2, v2)
VT_torch = X_tensor @ u2[:,0:1]
X_torch_rec = VT_torch @ u2[:, 0:1].T