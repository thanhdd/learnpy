import numpy
import _pickle as cPickle

def unpickle(file):   
    opened_file = open(file, 'rb')     
    dict = cPickle.load(opened_file, encoding='latin1')    
    opened_file.close()    
    return dict

def get_train_data():
    data_dir = 'cifar-10-batches-py/data_batch_'
    batch_data = [dict() for i in range(5)]
    batch_num = 5
    
    for i in range(batch_num):
      batch_data[i] = unpickle(data_dir + str(i + 1))
      if i == 0:       
        train_data = batch_data[i]['data'] # 10000x3072
        train_labels = batch_data[i]['labels'] # 1x10000
      else:
        train_data = numpy.append(train_data, batch_data[i]['data'],axis=0)
        train_labels = numpy.append(train_labels, batch_data[i]['labels'], axis=0)
    train_data = train_data.T # 3072x50000

    return train_data, train_labels

def get_test_data():
    test_batch_data = unpickle('cifar-10-batches-py/test_batch')
    test_data = test_batch_data['data'] # 10000x3072
    test_data = test_data.T
    test_labels = numpy.array(test_batch_data['labels'])
    
    return test_data, test_labels
def hello():
    print("hello")