# linearregression.py
import numpy
import matplotlib.pyplot as plt

X = numpy.array([[147, 150, 153, 158, 163, 165, 168, 170, 173, 175, 178, 180, 183]]).T
# weight (kg)
y = numpy.array([[ 49, 50, 51,  54, 58, 59, 60, 62, 63, 64, 66, 67, 68]]).T
# Visualize data 
plt.plot(X, y, 'r+')
plt.axis([140, 190, 45, 75])
plt.xlabel('Height (cm)')
plt.ylabel('Weight (kg)')
plt.show()

one = numpy.ones((X.shape[0], 1))
Xbar = numpy.concatenate((one, X), axis = 1)

A = numpy.dot(Xbar.T, Xbar)
b = numpy.dot(Xbar.T, y)
w = numpy.dot(numpy.linalg.pinv(A), b)
print('w= ', w)
w_0 = w[0][0]
w_1 = w[1][0]
x0 = numpy.linspace(145, 185, 2)
y0 = w_0 + w_1 * x0
plt.plot(X.T, y.T, 'r+')
plt.plot(x0, y0, 'b')
plt.axis([140, 190, 45, 75])
plt.xlabel('Height (cm)')
plt.ylabel('Weight (kg)')
plt.show()