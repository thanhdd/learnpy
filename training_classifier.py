# training_classifier.py
import torch
import torchvision
import torchvision.transforms as transforms
import matplotlib.pyplot as pyplot
import numpy
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
import time
# from torch.autograd import Variable
# **************************************** SCRIPTS **************************************************
    # torch.nn.Conv2d(input_channel,output_channel,filter_size,stride) # 
    # Example:
    # image: 32x32x3, 3 is input channels
    # input = 50000 x 3 x 32 x 32 # N = 50000, input_channel = 3 
    # model = torch.nn.Conv2d(3, 6, 5) # input channels = 3, output channels = 6, filter size = 5x5, stride = 1
    # output1 = 50000 x 6 x 28 x 28 
    # MaxPool2d(kernel_size=2,stride=2)
    # output2 50000 x 6 x 14 x 14
    # Conv2d(6,16,5)
    # output3 50000 x 16 x 10 x 10
    # # MaxPool2d(kernel_size=2,stride=2)
    # output4 50000 x 16 x 5 x 5
    # view -1
    # output5 50000 x 400
    # fc1 torch.nn.Linear(16*5*5, 120)
    # output6 50000 x 120
    # fc2 torch.nn.Linear(120, 84)
    # output7 50000 x 84
    # fc3 torch.nn.Linear(84, 10)
    # output8 50000 x 10

class Net(nn.Module):
  def __init__(self):
    super(Net, self).__init__()
    input_channels = 3 # 
    output_channels = 6 #kernel size or number of filter
    filter_size = 5 # 5 x 5
    self.conv1 = nn.Conv2d(input_channels, output_channels, filter_size)

    self.pool = nn.MaxPool2d(2, 2)

    input_channels = 6
    output_channels = 16
    self.conv2 = nn.Conv2d(input_channels, output_channels, filter_size)

    fc1_input = 16 * 5 * 5
    fc1_output = 120
    fc2_output = 84
    fc3_output = 10

    self.fc1 = nn.Linear(fc1_input, fc1_output)
    self.fc2 = nn.Linear(fc1_output, fc2_output)
    self.fc3 = nn.Linear(fc2_output, fc3_output)

  def forward(self, x):
    x = self.pool(F.relu(self.conv1(x)))
    x = self.pool(F.relu(self.conv2(x)))
    x = x.view(-1, 16 * 5 * 5) # Flatten to 400x1
    x = F.relu(self.fc1(x))
    x = F.relu(self.fc2(x))
    x = self.fc3(x)
    return x

# net = Net()

def imshow(image):
  image = image / 2 + 0.5
  numpy_image = image.numpy()
  pyplot.imshow(numpy.transpose(numpy_image, (1,2,0)))
  pyplot.show()

transform = transforms.Compose(
                              [transforms.ToTensor(),
                                transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5))])

trainset = torchvision.datasets.CIFAR10(root='./data', train=True, download=True, transform=transform)

trainloader = torch.utils.data.DataLoader(trainset, batch_size=4, shuffle=True)

test_set = torchvision.datasets.CIFAR10(root='./data', train=False, download=True, transform=transform)

testloader = torch.utils.data.DataLoader(test_set, batch_size=4, shuffle=False)

classes = ('plane', 'car', 'bird', 'cat', 'deer', 'dog', 'frog', 'horse', 'ship', 'truck')

data_iter = iter(trainloader)
images, labels = data_iter.next()

#show image
imshow(torchvision.utils.make_grid(images))

#print labels
print(' '.join('%5s' % classes[labels[j]] for j in range(4)))


#Convolution: 
# 
device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
# torch.backends.cudnn.deterministic = True
# print(device)

net = Net()
net = net.to(device)
# net = net.cuda()

# import torch.optim as optim

criterion = nn.CrossEntropyLoss()
optimizer = optim.SGD(net.parameters(), lr=0.001, momentum=0.9)

  # for i, data in enumerate(trainloader, 0):
  #   inputs, labels = data
  
    # inputs = inputs.cuda()
    # labels = labels.cuda()
    # inputs = Variable(inputs).cuda()
    # labels = Variable(labels).cuda()

begin = time.time()
for epoch in range(2):

  running_loss = 0.0
  for i, data in enumerate(trainloader,0):
    inputs, labels = data
    inputs, labels = inputs.to(device), labels.to(device)

    optimizer.zero_grad()

    outputs = net(inputs)
    loss = criterion(outputs, labels)
    loss.backward()
    optimizer.step()

    # print progress
    running_loss += loss.item()
    if i % 2000 == 1999:
      print('[%d, %5d] loss: %0.3f' %(epoch + 1, i + 1, running_loss / 2000))
      running_loss = 0.0

end = time.time()
print('Finished Training. It takes: %0.2f second' % (end - begin)) 

# 85s cpu, 

data_iter = iter(testloader)
images, labels = data_iter.next()
imshow(torchvision.utils.make_grid(images))
print('Ground Truth: ', ' '.join('%5s' % classes[labels[j]] for j in range(4)))

outputs = net(images)
_, predicted = torch.max(outputs, 1)
print('Predicted: ', ' '.join('%5s' % classes[predicted[j]] for j in range(4)))

# correct = 0
# total = 0
# with torch.no_grad():
#   for data in testloader:
#     images, labels = data
#     images, labels = images.to(device), labels.to(device)
#     outputs = net(images)
#     _, predicted = torch.max(outputs.data, 1)
#     total += labels.size(0)
#     correct += (predicted == labels).sum().item()

# print('Accuracy of the network on the 10000 test images: %d %%' % (100 * correct / total)) # 52%

# class_correct = list(0. for i in range(10))
# class_total = list(0. for i in range(10))
# with torch.no_grad():
#   for data in testloader:
#     images, labels = data
#     images, labels = images.to(device), labels.to(device)
#     outputs = net(images)
#     _, predicted = torch.max(outputs.data, 1)
#     c = (predicted == labels).squeeze()
#     for i in range(4):
#       label = labels[i]
#       class_correct[label] += c[i].item()
#       class_total[label] += 1

# for i in range(10):
#   print('Accuracy of %5s: %2d %%' % (classes[i], 100 * class_correct[i] / class_total[i]


# [1,  2000] loss: 2.198
# [1,  4000] loss: 1.901
# [1,  6000] loss: 1.699
# [1,  8000] loss: 1.590
# [1, 10000] loss: 1.521
# [1, 12000] loss: 1.477
# [2,  2000] loss: 1.399
# [2,  4000] loss: 1.396
# [2,  6000] loss: 1.359
# [2,  8000] loss: 1.306
# [2, 10000] loss: 1.297
# [2, 12000] loss: 1.259
# Finished Training
# Accuracy of the network on the 10000 test images: 54 %
# Accuracy of plane: 77 %
# Accuracy of   car: 69 %
# Accuracy of  bird: 28 %
# Accuracy of   cat: 45 %
# Accuracy of  deer: 22 %
# Accuracy of   dog: 25 %
# Accuracy of  frog: 79 %
# Accuracy of horse: 75 %
# Accuracy of  ship: 55 %
# Accuracy of truck: 62 %

